(defproject auth "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/tools.nrepl "0.2.13"]
                 [org.clojure/core.async "0.4.474"]
                 [org.clojure/core.cache "0.7.1"]
                 [org.clojure/data.json "0.2.6"]
                 [duct/core "0.6.2"]
                 [duct/module.logging "0.3.1"]
                 [duct/module.web "0.6.4"]
                 [duct/module.ataraxy "0.2.0"]
                 [duct/module.sql "0.4.2"]
                 [duct/handler.sql "0.3.1"]
                 [duct/database.sql.hikaricp "0.3.3"]
                 [com.stuartsierra/log.dev "0.2.0"]
                 [buddy/buddy-auth "2.1.0"]
                 [buddy/buddy-hashers "1.3.0"]
                 [org.mariadb.jdbc/mariadb-java-client "2.2.6"]
                 [clj-http "3.9.1"]
                 [clj-time "0.14.4"]
                 [yesql "0.5.3"]
                 [ring-cors "0.1.12"]
                 [failjure "1.3.0"]

                 [com.google.firebase/firebase-admin "6.2.0" :exclusions [io.grpc/grpc-core]]
                 [com.google.api-client/google-api-client "1.23.0" :exclusions [[com.fasterxml.jackson.core/jackson-core]]]
                 [com.google.http-client/google-http-client-jackson "1.23.0"]]
  :exclusions [org.slf4j/slf4j-nop]
  :plugins [[duct/lein-duct "0.10.6"]]
  :main ^:skip-aot auth.main
  :resource-paths ["resources" "target/resources"]
  :prep-tasks     ["javac" "compile" ["run" ":duct/compiler"]]
  :profiles
  {:dev  [:project/dev :profiles/dev]
   :repl {:prep-tasks   ^:replace ["javac" "compile"]
          :repl-options {:init-ns user}}
   :uberjar {:aot :all}
   :profiles/dev {}
   :project/dev  {:source-paths   ["dev/src"]
                  :resource-paths ["dev/resources"]
                  :dependencies   [[integrant/repl "0.2.0"]
                                   [eftest "0.4.1"]
                                   [kerodon "0.9.0"]]}})
