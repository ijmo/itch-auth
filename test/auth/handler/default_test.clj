(ns auth.handler.default-test
  (:require [clojure.test :refer :all]
            [integrant.core :as ig]
            [ring.mock.request :as mock]
            [auth.handler.default :as default]))

(deftest smoke-test
  (testing "example page exists"
    (let [handler  (ig/init-key :auth.handler.default/health {})
          response (handler (mock/request :get "/health"))]
      (is (= :ataraxy.response/ok (first response)) {:status "UP"}))))
