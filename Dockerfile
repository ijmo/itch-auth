# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

RUN apk add --update tzdata

# Add Maintainer Info
LABEL maintainer="ijmo"

# Add a volume pointing to /tmp
# VOLUME /tmp

# Make port 8080 available to the world outside this container
EXPOSE 8900

# The application's jar file
ARG JAR_FILE=target/auth-0.1.0-SNAPSHOT-standalone.jar

# Add the application's jar to the container
ADD ${JAR_FILE} itch-auth.jar

ENV PORT=8900

# Run the jar file 
ENTRYPOINT ["java","-jar","/itch-auth.jar"]
