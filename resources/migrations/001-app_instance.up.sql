CREATE TABLE itch_auth.app_instance (
    inst_id VARCHAR(36) PRIMARY KEY   -- App instance Id(UUID)
  , platform VARCHAR(1) NOT NULL      -- 'A': Android(GCM), 'I': iOS(Apple Push)
  , device_model VARCHAR(32)
  , created_on DATETIME NOT NULL
);
