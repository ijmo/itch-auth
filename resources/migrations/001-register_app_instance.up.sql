CREATE PROCEDURE register_app_instance ( IN
    _inst_id VARCHAR(36)
  , _platform VARCHAR(1)
  , _device_model VARCHAR(32)
)
BEGIN
  DECLARE $inst_id TYPE OF app_instance.inst_id DEFAULT NULL;

  START TRANSACTION;

    # Loop: Get a new uuid if it duplicates
    new_uuid: LOOP
      SELECT inst_id INTO $inst_id FROM app_instance WHERE inst_id=_inst_id;

      IF $inst_id IS NULL THEN
        LEAVE new_uuid;
      END IF;

      SELECT UUID(), NULL INTO _inst_id, $inst_id;
    END LOOP new_uuid;

    INSERT INTO app_instance (inst_id, platform, device_model, created_on)
                      VALUES (_inst_id, _platform, _device_model, NOW());
  COMMIT;
  SELECT _inst_id AS inst_id;
END;
