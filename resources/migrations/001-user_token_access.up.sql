CREATE TABLE user_token_access (
  user_id MEDIUMINT NOT NULL
  , inst_id VARCHAR(36) NOT NULL  -- App instance Id(UUID)
  , token VARCHAR(2000) NOT NULL  -- JWT
  , expire_on DATETIME NOT NULL
  , created_on DATETIME NOT NULL
);
