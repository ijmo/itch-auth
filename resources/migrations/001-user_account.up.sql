CREATE TABLE itch_auth.user_account (
  user_id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT
  , user_key BIGINT UNSIGNED
  , pw VARCHAR(200) -- hashed
  , created_on DATETIME NOT NULL
  , updated_on DATETIME NOT NULL
);
