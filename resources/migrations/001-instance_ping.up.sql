CREATE TABLE itch_auth.instance_ping (
  inst_id VARCHAR(36)
  , user_key BIGINT UNSIGNED -- user_id logged in
  , ping_on DATETIME NOT NULL
  , extra VARCHAR(128)
);
