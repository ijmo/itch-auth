CREATE PROCEDURE insert_ping ( IN
    _inst_id VARCHAR(36)
  , _user_key BIGINT UNSIGNED
  , _extra VARCHAR(128)
)
BEGIN
  INSERT INTO instance_ping (inst_id, user_key, extra, ping_on)
                     VALUES (_inst_id, _user_key, _extra, NOW());
  SELECT _inst_id AS inst_id;
END;
