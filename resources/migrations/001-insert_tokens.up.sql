CREATE PROCEDURE insert_tokens ( IN
    _user_id INT UNSIGNED
  , _inst_id VARCHAR(36)
  , _refresh_token VARCHAR(2000)
  , _refresh_token_expire_on MEDIUMINT UNSIGNED
  , _access_token VARCHAR(2000)
  , _access_token_expire_on MEDIUMINT UNSIGNED
)
BEGIN
  INSERT INTO user_token_refresh
         (user_id, inst_id, token, expire_on, created_on)
  VALUES (_user_id, _inst_id, _refresh_token, _refresh_token_expire_on, NOW());

  INSERT INTO user_token_access
         (user_id, inst_id, token, expire_on, created_on)
  VALUES (_user_id, _inst_id, _access_token, _access_token_expire_on, NOW());

  SELECT _user_id AS user_id,
         _inst_id AS inst_id,
         _refresh_token AS refresh_token,
         _access_token AS access_token;
END;
