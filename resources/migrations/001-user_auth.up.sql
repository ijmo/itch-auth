CREATE TABLE itch_auth.user_auth (
  user_id INT UNSIGNED NOT NULL
  , origin VARCHAR(1) NOT NULL -- 'G': Google, 'F': Facebook, 'P': Phone, 'E': Email
  , auth_id VARCHAR(128) NOT NULL
  , created_on DATETIME NOT NULL
);
