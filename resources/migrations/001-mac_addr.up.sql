CREATE TABLE itch_auth.mac_addr (
  mac_addr VARCHAR(17) NOT NULL
  , created_on DATETIME NOT NULL
  , updated_on DATETIME NOT NULL
);
