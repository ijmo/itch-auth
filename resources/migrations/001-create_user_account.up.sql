CREATE PROCEDURE create_user_account ( IN
    _auth_id VARCHAR(64)
  , _origin VARCHAR(1)
  , _pw VARCHAR(200)
  , _name VARCHAR(40)
)
BEGIN
  DECLARE $user_id TYPE OF user_account.user_id DEFAULT NULL;
  DECLARE $user_key TYPE OF user_account.user_key DEFAULT NULL;

  START TRANSACTION;
    SELECT user_id
      INTO $user_id
      FROM user_auth
     WHERE auth_id=_auth_id
       AND origin=_origin
     LIMIT 1;

    IF $user_id IS NULL THEN
      INSERT INTO user_account (user_key, pw, created_on, updated_on)
                        VALUES (uuid_short(), _pw, NOW(), NOW());

      SELECT user_id, user_key INTO $user_id, $user_key
        FROM user_account
       WHERE user_id = last_insert_id()
       LIMIT 1;

      INSERT INTO user_auth (user_id, origin, auth_id, created_on)
                     VALUES ($user_id, _origin, _auth_id, NOW());

      INSERT INTO user_profile (user_id, name, created_on, updated_on)
           VALUES ($user_id, _name, NOW(), NOW());
    END IF;
  COMMIT;

  SELECT $user_id AS user_id, $user_key AS user_key;

END;
