CREATE TABLE itch_auth.user_profile (
  user_id INT UNSIGNED NOT NULL
  , name VARCHAR(40)
  , sex VARCHAR(1) -- M, F
  , birth_year VARCHAR(4)
  , birth_month VARCHAR(2)
  , birth_date VARCHAR(2)
  , disease VARCHAR(200) -- bar|joined
  , created_on DATETIME NOT NULL
  , updated_on DATETIME NOT NULL
);
