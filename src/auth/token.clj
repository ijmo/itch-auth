(ns auth.token
  (:require [integrant.core :as ig]
            [clj-time.core :as time]
            duct.database.sql
            [buddy.core.nonce :as nonce]
            [buddy.sign.jwt :as jwt]
            [buddy.hashers :as hashers])
  (:import java.util.Base64))


(defmethod ig/init-key ::refresh-token-ttl [_ {:keys [minutes]}] minutes)
(defmethod ig/init-key ::access-token-ttl [_ {:keys [minutes]}] minutes)


(def secret (nonce/random-bytes 32))


(defn encode-urlsafe-base64
  [bytes]
  (.encodeToString (.withoutPadding (Base64/getUrlEncoder)) bytes))


(defn decode-urlsafe-base64
  [bytes]
  (.decode (Base64/getUrlDecoder) bytes))


(defn issue-token
  ([id inst-id exp]           ;; for OAuth
   (let [claims {:user-id id
                 ;; :email (keyword email)
                 ;; :origin origin
                 :inst-id inst-id
                 ;; :platform platform
                 :exp (time/plus (time/now) (time/minutes exp))}
         secret (decode-urlsafe-base64 "Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=")
         token (jwt/sign claims "abcdefghijklmnopabcdefghijklmnop")] ;(jwt/encrypt claims secret {:alg :a256kw :enc :a128gcm})
     token)))
