(ns auth.firebase
  (:require [clojure.java.io :as io]
            [integrant.core :as ig]
            [duct.logger :as logger])
  (:import (com.google.firebase FirebaseApp
                                FirebaseOptions
                                FirebaseOptions$Builder)
           (com.google.auth.oauth2 GoogleCredentials)))


(defmethod ig/init-key ::app [_ {:keys [logger]}]
  (logger/log logger :info ::app "init firebase-app")
  (let [service-key-path "itch-tector-firebase-adminsdk-fetyz-9c96315bfb.json"
        service-account (.openStream (io/resource service-key-path))
        firebase-opts (-> (FirebaseOptions$Builder.)
                          (.setCredentials (GoogleCredentials/fromStream service-account))
                          (.setDatabaseUrl "http://itch-tector.firebaseio.com/")
                          (.build))]
    {:app (FirebaseApp/initializeApp firebase-opts)
     :logger logger}))


(defmethod ig/halt-key! ::app [_ {:keys [app logger]}]
  (logger/log logger :info ::app "stop firebase-app")
  (.delete app))
