(ns auth.settings
  (:require [integrant.core :as ig]
            [ring.middleware.cors :refer [wrap-cors]]
            [clojure.tools.nrepl.server :refer [start-server]]
            [duct.logger :as logger]))


(defmethod ig/init-key ::timezone [_ _]
  (java.util.TimeZone/getTimeZone "Asia/Seoul"))


(defmethod ig/init-key ::cors-middleware [_ options]
  #(wrap-cors % :access-control-allow-origin [#"^.*$"]
              :access-control-allow-methods [:get :put :post :delete]))

(defmethod ig/init-key ::repl [_ {:keys [port logger]}]
  (when (> port -1)
    (logger/log logger :info ::starting-repl {:port port})
    (start-server :port port :bind "0.0.0.0")))
