(ns auth.handler.default
  (:require [ataraxy.core :as ataraxy]
            [ataraxy.response :as response] 
            [integrant.core :as ig]
            [clojure.data.json :as json]
            [duct.logger :as logger]))


(defmethod ig/init-key ::health [_ options]
  (fn [{[_] :ataraxy/result}]
    [::response/ok {:status "UP"}]))


(defmethod ig/init-key ::echo [_ {:keys [logger]}]
  (fn [params]
    ;; (println "----- echo -----")
    ;; (clojure.pprint/pprint (select-keys params [:headers :body-params]))
    (logger/log logger :info ::echo (select-keys params [:headers :body-params]))    
    [::response/ok (json/write-str (select-keys params [:headers :body-params]))]))
