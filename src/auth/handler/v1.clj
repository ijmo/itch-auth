(ns auth.handler.v1
  (:require [clojure.string :refer [index-of]]
            [ataraxy.core :as ataraxy]
            [ataraxy.response :as response]
            [integrant.core :as ig]
            [duct.logger :as logger]
            [auth.boundary.app-instance :refer [insert-app-instance-id
                                                insert-instance-ping]]
            [auth.token :refer [issue-token]]
            [auth.boundary.token :refer [insert-tokens]]
            [auth.boundary.user :refer [create-user-account
                                        fetch-user-account]])
  (:import (com.google.firebase.auth FirebaseAuth
                                     FirebaseAuthException)))


(defmethod ig/init-key ::register-instance [_ {:keys [db logger]}]
  (fn [{[_ inst-id platform device-model] :ataraxy/result}]
    (let [res (insert-app-instance-id db inst-id platform device-model)
          inst-id (:inst_id res)]
      (logger/log logger :info ::register-instance {:inst-id inst-id
                                                    :platform platform
                                                    :device-model device-model
                                                    :result res})
      [::response/ok {:instanceId inst-id}])))


(defmethod ig/init-key ::ping  [_ {:keys [db logger]}]
  (fn [{[_ inst-id user-key extra] :ataraxy/result}]
    (let [res (insert-instance-ping db inst-id user-key extra)]
      (logger/log logger :info ::ping {:inst-id inst-id
                                       :user-key user-key
                                       :extra extra
                                       :result res})
      [::response/ok {:result "pong"}])))


(defmethod ig/init-key ::firebase-login [_ {:keys [db logger refresh-token-ttl access-token-ttl]}]
  (fn [{[_ inst-id id-token provider] :ataraxy/result}]
    (try
      (let [decoded-token (.verifyIdToken (FirebaseAuth/getInstance) id-token)
            ;; _ (println "## decoded: "(.getClaims decoded-token))
            origin (let [find-fn #(index-of provider %)]
                     (cond
                       (find-fn "google") "G"
                       (find-fn "facebook") "F"
                       (find-fn "phone") "P"
                       (find-fn "password") "E"))
            email (.getEmail decoded-token)
            uid (.getUid decoded-token)
            account (create-user-account db email origin "" "")
            user-id (:user_id account)
            user-key (:user_key account)            
            refresh-token (issue-token user-id inst-id refresh-token-ttl)
            access-token (issue-token user-id inst-id access-token-ttl)
            ;; _ (println "user-id=" user-id ", user-key=" user-key)
            res (insert-tokens db
                               user-id
                               inst-id
                               refresh-token
                               refresh-token-ttl
                               access-token
                               refresh-token-ttl)]
        (logger/log logger :info ::firebase-login {:id-token id-token
                                                   :inst-id inst-id
                                                   :provider provider
                                                   :user-id user-id
                                                   :user-key user-key
                                                   :refresh-token refresh-token
                                                   :access-token access-token})
        [::response/ok {:data {:refreshToken refresh-token
                               :accessToken access-token
                               :userKey (str user-id)}}])
      (catch FirebaseAuthException e
        (logger/log logger :error ::firebase-login {:message (.getMessage e)})
        [::response/ok {:data "get fresh token!!!!!!!"}])
      (catch Exception e
        (logger/log logger :error ::firebase-login {:message (.getMessage e)})
        [::response/ok {:data "invalid format??????"}]))))


(defmethod ig/init-key ::new-access-token [_ {:keys [db logger access-token-ttl]}]
  (fn [{[_ user-id inst-id] :ataraxy/result}]
    (if-let [account (fetch-user-account db user-id)]
      (when-let [access-token (issue-token user-id inst-id access-token-ttl)]
        
        ;; refresh-token (issue-token user-id email origin inst-id platform refresh-token-ttl)
        ;; access-token (issue-token user-id email origin inst-id platform access-token-ttl)
        ;; res (insert-tokens db
        ;;                             user-id
        ;;                             inst-id
        ;;                             refresh-token
        ;;                             refresh-token-ttl
        ;;                             access-token
        ;;                             refresh-token-ttl)
        (logger/log logger :info ::new-access-token {:user-id user-id
                                                     :inst-id inst-id
                                                     :access-token access-token})
        [::response/ok {:data {:accessToken access-token}}])
      [::response/unauthorized {:data nil}])))










