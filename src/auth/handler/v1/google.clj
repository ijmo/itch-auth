(ns auth.handler.v1.google
  (:require [ataraxy.core :as ataraxy]
            [ataraxy.response :as response]
            [integrant.core :as ig]
            [failjure.core :as f]
            [auth.token :refer [issue-token]]
            [auth.boundary.token :refer [insert-tokens]]
            [auth.boundary.user :refer [create-user-account]])
  (:import (java.util Collections)
           (com.google.api.client.googleapis.auth.oauth2 GoogleIdToken
                                                         GoogleIdToken$Payload
                                                         GoogleIdTokenVerifier
                                                         GoogleIdTokenVerifier$Builder)
           (com.google.api.client.http.javanet NetHttpTransport)
           (com.google.api.client.json.jackson JacksonFactory)))

;; (defprotocol Users
;;   (create-user [db email password])
;;   )


;; (extend-protocol Users
;;   duct.database.sql.Boundary
;;   (create-user [{db :spec} email password]
;;     (let [pw-hash (hashers/derive password)
;;           results (jdbc/insert! db :users {:email email, :password pw-hash})]
;;       (-> results ffirst val))))

(defonce client-id "20148347273-kv8104tdt1e64877urpuemipach1vm7p.apps.googleusercontent.com")


(defn verify-token [id-token]
  (let [transport (NetHttpTransport.)
        json-factory (JacksonFactory.)
        verifier (-> (GoogleIdTokenVerifier$Builder. transport json-factory)
                     (.setAudience (Collections/singletonList client-id))
                     (.setIssuer "https://accounts.google.com")
                     (.build))]
    (try
      (if-let [goog-id-token (.verify verifier id-token)]
        goog-id-token
        (f/fail "Failed to Verify:" id-token))
      (catch Exception e
        (f/fail "Invalid Token:" id-token)))))


(defmethod ig/init-key ::login [_ {:keys [db logger refresh-token-ttl access-token-ttl]}]
  (fn [{[_ inst-id platform id-token] :ataraxy/result}]
    ;; Check aud(web id)
    ;; azp(app id)
    ;; email
    (println "welcome" id-token)
    (if-let [goog-id-token (verify-token id-token)]
      (let [_ (println goog-id-token)
                      payload (.getPayload goog-id-token)
                      email (.getEmail payload)
                      origin "G"
                      pw nil
                      account (create-user-account db email origin pw "")
                      user-id (:user_id account)
                      refresh-token (issue-token user-id platform refresh-token-ttl)
                      access-token (issue-token user-id platform access-token-ttl)
                      res (insert-tokens db
                                         user-id
                                         inst-id
                                         refresh-token
                                         refresh-token-ttl
                                         access-token
                                         refresh-token-ttl)]
                     (println "result:" res)
        [::response/ok {:data {:refreshToken refresh-token
                               :accessToken access-token}}])
      [::response/unauthorized {:error "Verify Failed"}])))


