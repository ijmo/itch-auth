(ns auth.handler.v1.facebook
  (:require [ataraxy.core :as ataraxy]
            [ataraxy.response :as response]
            [integrant.core :as ig]
            [taoensso.timbre :as timbre :refer [tracef debugf infof warnf errorf]]
            [clj-http.client :as client]
            [auth.token :refer [issue-token]]
            [auth.boundary.token :refer [insert-tokens]]
            [auth.boundary.user :refer [create-user-account]]))


(defonce app-token "173812916793533|ZTtKZW8NPIoihKrtwbFwm5SWd2Y")
(defonce app-id "173812916793533")
(defonce app-secret "3cf7b709bd6a34c82649af8b81fe43cd")


(defn verify-token [token]
  (let [resp (client/get "https://graph.facebook.com/debug_token"
                         {:query-params {"input_token" token
                                         "access_token" app-token}
                          :accept :json
                          :as :json})]
    (println "FB Received:" (:body resp))))


(defn get-profile [token & keys]
  (let [resp (client/get "https://graph.facebook.com/v3.1/me"
                         {:query-params {"fields" (clojure.string/join "," (if keys (map name keys) []))
                                         "access_token" token}
                          :accept :json
                          :as :json})]
    (print "\n\nget-email result:")
    (println (select-keys (:body resp) keys))
    (select-keys (:body resp) keys)))


(defn exchange-token [token]
  (let [resp (client/get "https://graph.facebook.com/v3.1/oauth/access_token"
                         {:query-params {"grant_type" "fb_exchange_token"
                                         "client_id" app-id
                                         "client_secret" app-secret
                                         "fb_exchange_token" token}
                          :accept :json
                          :as :json})]
    (println "\n\nexchange result:")
    (println (:body resp))
    (-> resp :body :access_token)))


(defmethod ig/init-key ::login [_ {:keys [db refresh-token-ttl access-token-ttl]}]
  (fn [{[_ inst-id platform user-token] :ataraxy/result}]
    (let [verified (verify-token user-token)
          long-live-token (future (exchange-token user-token))
          profile (future (get-profile user-token :id :name :email))
          email (:email @profile)
          origin "F"
          pw nil
          account (create-user-account db email origin pw "")
          user-id (:user_id account)
          refresh-token (issue-token user-id inst-id refresh-token-ttl)
          access-token (issue-token user-id inst-id access-token-ttl)
          res (insert-tokens db
                             user-id
                             inst-id
                             refresh-token
                             refresh-token-ttl
                             access-token
                             refresh-token-ttl)]
      (println "result:" res)
      [::response/ok {:data {:refreshToken refresh-token
                             :accessToken access-token}}])
    [::response/ok {:message "facebook login"}]))


