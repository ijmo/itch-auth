(ns auth.boundary.token
  (:require [integrant.core :as ig]
            [clojure.java.jdbc :as jdbc]
            duct.database.sql)
  (:import java.util.Base64))


(defprotocol UserToken
  (insert-tokens [db user-id inst-id refresh-token refresh-token-ttl access-token access-token-ttl]))


(extend-protocol UserToken
  duct.database.sql.Boundary

  (insert-tokens
    [{db :spec} user-id inst-id refresh-token refresh-token-ttl access-token access-token-ttl]
    (let [results (jdbc/query db ["{call INSERT_TOKENS(?,?,?,?,?,?)}"
                                  user-id inst-id refresh-token refresh-token-ttl
                                  access-token access-token-ttl])
          res (-> results first)]
      res)))
