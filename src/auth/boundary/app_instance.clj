(ns auth.boundary.app-instance
  (:require [integrant.core :as ig]
            [clojure.java.jdbc :as jdbc]
            duct.database.sql))


(defprotocol AppInstance
  (insert-app-instance-id [db inst-id platform extra])
  (insert-instance-ping [db inst-id user-key extra]))


(extend-protocol AppInstance
  duct.database.sql.Boundary

  (insert-app-instance-id
    [{db :spec} inst-id platform device-model]
    (let [results (jdbc/query db ["{call REGISTER_APP_INSTANCE(?,?,?)}" inst-id platform device-model])
          res (-> results first)]
      res))

  (insert-instance-ping
    [{db :spec} inst-id user-key extra]
    (let [user-key (if (> user-key 0) user-key nil)
          results (jdbc/query db ["{call INSERT_PING(?,?,?)}" inst-id user-key extra])
          res (-> results first)]
      res)))
