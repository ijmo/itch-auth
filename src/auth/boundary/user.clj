(ns auth.boundary.user
  (:require [clojure.java.jdbc :as jdbc]
            duct.database.sql))


(defprotocol UserAccount
  (create-user-account [db email origin pw uname])
  (fetch-user-account [db user-id]))


(extend-protocol UserAccount
  duct.database.sql.Boundary

  (create-user-account
    [{db :spec} email origin pw uname]
    (let [results (jdbc/query db ["{call CREATE_USER_ACCOUNT(?,?,?,?)}" email origin pw uname])
          res (-> results first)]
      res))

  (fetch-user-account [{db :spec} user-id]
    (let [results (jdbc/query db ["select user_id, origin, auth_id from user_auth where user_id = ?" user-id])
          res (-> results first)]
      res)))
